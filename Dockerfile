FROM nginx:alpine
# FROM node:alpine

COPY default.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

WORKDIR /usr/share/nginx/html

COPY dist/leandrobarral-com-br/ .
